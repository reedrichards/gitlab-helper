Changelog
=========


(0.2.16)
------------
- Merge branch 'master' of gitlab.com:reedrichards/gitlab-helper.
  [robert.wendt]
- Merge branch 'glb-add-group-flag' into 'master' [Robert]

  Glb add group flag

  See merge request reedrichards/gitlab-helper!1
- Still need create project function for legacy code. [robert.wendt]
- Test for add new project with group feature. [robert.wendt]
- Group list fixture. [robert.wendt]
- Add project by group if group is supplied. [robert.wendt]
- Adds aility to add projects to group. [robert.wendt]


0.2.15 (2019-07-07)
-------------------
- Version updated from 0.2.14 to 0.2.15. [robert.wendt]
- Removed commented code, not gonna use. [robert.wendt]


0.2.14 (2019-07-05)
-------------------
- Version updated from 0.2.13 to 0.2.14. [robert.wendt]
- Flake8. [robert.wendt]
- Version updated from 0.2.12 to 0.2.13. [robert.wendt]
- Added usage docstring for new. [robert.wendt]
- Update to add new typescrip projects. [robert.wendt]
- Added typescript project config. [robert.wendt]
- Update docs. [robert.wendt]
- Merge remote-tracking branch 'origin/master' [robert.wendt]
- Add develpment to manifet. [robert.wendt]
- Some instructions on command layout. [robert.wendt]
- Added develpment to index. [robert.wendt]
- Renames origin to old origin if origin exist. [robert.wendt]
- Added development comment. [robert.wendt]


0.2.12 (2019-06-22)
-------------------
- Version updated from 0.2.11 to 0.2.12. [robert.wendt]
- Import now exists. [robert.wendt]
- Formatting. [robert.wendt]
- Added doc and type annotations. [robert.wendt]
- Added some unit tests that need fixtures. [robert.wendt]
- Added some intergration tests. [robert.wendt]
- Raise error instead of just returning. [robert.wendt]
- Formatting. [robert.wendt]
- Add coverage. [robert.wendt]
- Flake8. [robert.wendt]
- Removed bad comment. [robert.wendt]
- Responses is a dependency now. [robert.wendt]
- Removed crappy test. [robert.wendt]
- Added some tests. [robert.wendt]
- Added some fixtures. [robert.wendt]


0.2.11 (2019-06-16)
-------------------
- Version updated from 0.2.10 to 0.2.11. [robert.wendt]
- Fix makefile. [robert.wendt]
- Rough quickstart guide. [robert.wendt]
- Not needed (i think) [robert.wendt]
- Dependencies don't work. [robert.wendt]
- Continue updating. [robert.wendt]
- Htmlcov artifact? [robert.wendt]
- Dependencies. [robert.wendt]
- Starting readme. [robert.wendt]
- Docs artifact. [robert.wendt]
- Ci coverage back, and retrying docs logic. [robert.wendt]
- Install git. [robert.wendt]
- Reorder for faster deployment. [robert.wendt]
- Updated links to docs & coverage. [robert.wendt]
- Only master for docs. [robert.wendt]
- Build docs. [robert.wendt]
- Make docs also makes coverage. [robert.wendt]
- Added k8s manifest. [robert.wendt]
- Added dockerfile. [robert.wendt]
- See why my page isn't displaying. [robert.wendt]
- Gitlab doc link. [robert.wendt]
- Updated documentation link. [robert.wendt]
- Rename job to pages. [robert.wendt]
- Pypi status badge. [robert.wendt]
- Python version badge. [robert.wendt]
- More formatting. [robert.wendt]
- Formatting. [robert.wendt]
- Gitinspector is a pain, do it l8r. [robert.wendt]
- Added downloads badge. [robert.wendt]
- License badge. [robert.wendt]
- Global install? [robert.wendt]
- Creae gitinspector dir. [robert.wendt]
- Removed gitinspector, try giinspector command again. [robert.wendt]
- Proper path gitinspector. [robert.wendt]
- Added reports, cov, and docs proper path. [robert.wendt]
- Add docs pipeline. [robert.wendt]
- Coverage try 2. [robert.wendt]
- Alt tags. [robert.wendt]
- Added pipeline badge. [robert.wendt]
- Trying to add coverage badge. [robert.wendt]
- Added gitinspector file. [robert.wendt]
- Added gitinspector, releaseing builds docs. [robert.wendt]
- Remove travis. [robert.wendt]


0.2.10 (2019-06-16)
-------------------
- Version updated from 0.2.9 to 0.2.10. [robert.wendt]
- No more lazy_import. [robert.wendt]
- Not github. [robert.wendt]
- Vscode out of source control. [robert.wendt]
- Flake8. [robert.wendt]
- Throwing everything in here for now. [robert.wendt]
- Remove from base to prevent circular imports. [robert.wendt]
- Import path. [robert.wendt]
- No longer needed. [robert.wendt]
- Import issues solved. [robert.wendt]
- Name. [robert.wendt]
- Names. [robert.wendt]
- Docs. [robert.wendt]
- Rename project. [robert.wendt]
- Rename project. [robert.wendt]
- Rename project. [robert.wendt]


0.2.9 (2019-06-16)
------------------
- Version updated from 0.2.8 to 0.2.9. [robert.wendt]
- Changelog update to contain version instead of 0.2.16.
  [robert.wendt]


0.2.8 (2019-06-16)
------------------
- Version updated from 0.2.7 to 0.2.8. [robert.wendt]
- Update changelog. [robert.wendt]
- Update changelog. [robert.wendt]
- Commit changelog before push. [robert.wendt]
- Flake8. [robert.wendt]


0.2.7 (2019-06-16)
------------------
- Version updated from 0.2.6 to 0.2.7. [robert.wendt]


0.2.6 (2019-06-16)
------------------
- Version updated from 0.2.5 to 0.2.6. [robert.wendt]
- Changelog. [robert.wendt]
- Versioning and changelog. [robert.wendt]


0.2.5 (2019-06-16)
------------------
- Version updated from 0.2.4 to 0.2.5. [robert.wendt]


0.2.4 (2019-06-16)
------------------
- Version updated from 0.2.3 to 0.2.4. [robert.wendt]
- Tags master deploys. [robert.wendt]


0.2.3 (2019-06-16)
------------------
- Version updated from 0.2.2 to 0.2.3. [robert.wendt]
- Pass env vars to test env. [robert.wendt]
- Tags && master. [robert.wendt]


0.2.2 (2019-06-16)
------------------
- Version updated from 0.2.1 to 0.2.2. [robert.wendt]
- Oops. [robert.wendt]


0.2.1 (2019-06-16)
------------------
- Version updated from 0.2.0 to 0.2.1. [robert.wendt]
- Requirements should be sane ish now. [robert.wendt]
- Import path. [robert.wendt]
- Flake8. [robert.wendt]
- First bump. [robert.wendt]


0.2.0 (2019-06-16)
------------------
- Version updated from 0.1.0 to 0.2.0. [robert.wendt]
- Messing with build. [robert.wendt]
- Adding better version bumping ability. [robert.wendt]
- Added glb add command. [robert.wendt]
- Add not git. [robert.wendt]
- Need a git executeable. [robert.wendt]
- Updated test ci. [robert.wendt]
- Various updates, released first build. [robert.wendt]


0.1.0 (2019-06-10)
------------------
- Init. [robert.wendt]


