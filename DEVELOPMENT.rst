Development
===========

Designing New Commands
----------------------

Commands for gitlab helper are designed using a VERB NOUN syntax.

eg.:

.. code-block:: shell

    # command VERB NOUN
      glb     new  project # args
