#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `glb` package."""

import pytest  # noqa: F401
from random import randint
from click.testing import CliRunner

from gitlab_helper import cli
from gitlab_helper.__init__ import __version__


def test_command_line_interface():
    """Test the CLI."""
    runner = CliRunner()
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    assert "glb" in result.output
    help_result = runner.invoke(cli.main, ["--help"])
    assert help_result.exit_code == 0
    assert "--help  Show this message and exit." in help_result.output


def test_version():
    """Test the CLI."""
    runner = CliRunner()
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    assert "glb" in result.output
    version_result = runner.invoke(cli.main, ["version"])
    assert version_result.exit_code == 0
    assert __version__ in version_result.output


def test_new_project():
    """Test the CLI."""
    runner = CliRunner()
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    assert "glb" in result.output
    rand = str(randint(999, 1001)) + "-" + str(randint(999, 10001))
    project_name = "test_" + rand
    new_project_result = runner.invoke(cli.main, ["new"])
    assert 'Error: Missing argument "PROJECT"' in new_project_result.output
    new_project_result = runner.invoke(cli.main, ["new", "cli", project_name])
    assert result.exit_code == 0


def test_new_project_list():
    """Test that new project lists all project"""
    runner = CliRunner()
    result = runner.invoke(cli.main, ["new", "--list"])
    assert result.exit_code == 0
