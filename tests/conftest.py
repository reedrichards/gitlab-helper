#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""`gitlab_helper` test configuration file

Available Fixtures:

    group_list: list of gropus from the configured gitlab account

Example:

    write a fixture

        ``
        @pytest.fixture()
        def hello_world():
            return 'Hello World!'
        ``

    use a fixture
        ``
        import pytest
        from hello.main import hello

        def test_hello(hello_world):
           hello_world_expected = hello("world")
           assert hello_world_expected == hello_world
        ``

Todo:
    *
    * You have to also use ``sphinx.ext.todo`` extension
"""
import pytest  # noqa: F401
import responses
from gitlab_helper.main import my_groups


@pytest.fixture
def project_create_response():
    with responses.RequestsMock() as rsps:
        rsps.add(
            responses.POST,
            "https://gitlab.com/api/v4/projects",
            # json=[
            #     {
            #         "avatarurl": None,
            #         "description": "burnout.ai is a com... platform",
            #         "fullname": "burnout",
            #         "fullpath": "burnout",
            #         "id": 3927011,
            #         "ldapaccess": None,
            #         "ldapcn": None,
            #         "lfsenabled": True,
            #         "name": "burnout",
            #         "parentid": None,
            #         "path": "burnout",
            #         "requestaccessenabled": False,
            #         "visibility": "private",
            #         "weburl": "https://gitlab.com/...s/burnout",
            #     }
            # ],
            status=201,
            content_type="application/json",
        )
        yield rsps


@pytest.fixture
def groups_response():
    with responses.RequestsMock() as rsps:
        rsps.add(
            responses.GET,
            "https://gitlab.com/api/v4/groups",
            json=[
                {
                    "avatarurl": None,
                    "description": "burnout.ai is a com... platform",
                    "fullname": "burnout",
                    "fullpath": "burnout",
                    "id": 3927011,
                    "ldapaccess": None,
                    "ldapcn": None,
                    "lfsenabled": True,
                    "name": "burnout",
                    "parentid": None,
                    "path": "burnout",
                    "requestaccessenabled": False,
                    "visibility": "private",
                    "weburl": "https://gitlab.com/...s/burnout",
                }
            ],
            status=200,
            content_type="application/json",
        )
        yield rsps


@pytest.fixture
def groups_list():
    return my_groups()
