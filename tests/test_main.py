#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""tests for `gitlab_helper.main`



Example:
    run this test file

        $ py.test tests.test_main

   run one test

        $ py.test tests.test_main::gid_from_value

Attributes
    None

Todo:
    * You have to also use ``sphinx.ext.todo`` extension
    * fixturize test_crate_project
"""
import pytest  # noqa: F401
from random import randint

from gitlab_helper.main import (
    gid_from_value,
    my_groups,
    create_project,
    add_new_project,
)


def test_add_new_project(groups_list):
    gid = gid_from_value("react-typescript", groups_list.json())
    rand = str(randint(1, 999)) + "-" + str(randint(1, 101))
    # adding project to groups works
    add_new_project("test-groups" + rand, gid)
    rand = str(randint(1, 999)) + "-" + str(randint(1, 101))
    project_name = "test_" + rand
    project = add_new_project(project_name, False)
    assert project.name == project_name


def test_create_project():
    """
    eventually going to pint this away from my project for test to a self
    hosted gl instance. for now just suffer and tear down
    """
    rand = str(randint(1, 999)) + "-" + str(randint(1, 101))
    project_name = "test_" + rand
    project = create_project(project_name)
    assert project.name == project_name


def test_my_groups(groups_response):
    gr = my_groups()
    assert gr.status_code == 200


def test_gid_from_value(groups_list):
    gid = gid_from_value("burnout", groups_list.json())
    assert gid == 3927011
