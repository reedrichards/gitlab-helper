# -*- coding: utf-8 -*-

"""Top-level package for gitlab_helper."""

__author__ = """Robert Wendt"""
__email__ = 'rwendt1337@gmail.com'
__version__ = '0.2.16'
