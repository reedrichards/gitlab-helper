#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""
import os
import re
from setuptools import setup, find_packages


def get_version(*file_paths):
    """Retrieves the version from pf_emails/__init__.py"""
    filename = os.path.join(os.path.dirname(__file__), *file_paths)
    version_file = open(filename).read()
    version_match = re.search(
        r"^__version__ = ['\"]([^'\"]*)['\"]", version_file, re.M
    )
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


version = get_version("gitlab_helper", "__init__.py")

readme = open("README.rst").read()

setup_requirements = ["pytest-runner"]

test_requirements = ["pytest"]

setup(
    author="Robert Wendt",
    author_email="rwendt1337@gmail.com",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.7",
    ],
    description="some useful actions for interacting with gitlab",
    long_description=readme,
    entry_points={"console_scripts": ["glb=gitlab_helper.cli:main"]},
    install_requires=[
        "Click>=6.0",
        "gitpython",
        "python-gitlab",
        "cookiecutter",
    ],
    license="MIT license",
    include_package_data=True,
    keywords="gitlab_helper",
    name="gitlab-helper",
    packages=find_packages(),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://github.com/reedrichards/glb",
    version=version,
    zip_safe=False,
)
